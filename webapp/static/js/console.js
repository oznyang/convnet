$(function () {
    if ($('table.list-table').size()) {
        $('table.list-table tr:odd').addClass('odd');
        $('table.list-table tr:even').addClass('even');
        $("table.list-table tr").hover(
            function () {
                $(this).addClass("hover");
            },
            function () {
                $(this).removeClass("hover");
            }
        )
    }
    $('form').validationEngine();
});
