package net.convnet.server.ex;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public class CodecException extends ConvnetException {
    private static final long serialVersionUID = -1974937277693378901L;

    public CodecException(Object obj) {
        super(obj.toString(), CODEC_ERROR);
    }
}
