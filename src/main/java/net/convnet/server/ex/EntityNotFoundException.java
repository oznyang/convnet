package net.convnet.server.ex;

import java.io.Serializable;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-5-10
 */
public class EntityNotFoundException extends EntityException {
    private static final long serialVersionUID = 5977817365829480718L;

    public EntityNotFoundException(Class clazz, String msg, Throwable cause) {
        super(cause, ENTITY_NOT_FOUND, clazz.getSimpleName(), msg);
    }

    public EntityNotFoundException(Class clazz, String msg) {
        this(clazz, msg, null);
    }

    public EntityNotFoundException(Class clazz, String key, Serializable value) {
        this(clazz, key + "=" + value);
    }
}
