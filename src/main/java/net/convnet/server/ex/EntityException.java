package net.convnet.server.ex;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public class EntityException extends ConvnetException {
    private static final long serialVersionUID = -4686633812095775678L;

    public EntityException(Class clazz, String msg, Throwable cause) {
        super(cause, ENTITY_ERROR, clazz.getSimpleName(), msg);
    }

    public EntityException(Class clazz, String msg) {
        this(clazz, msg, null);
    }

    public EntityException(Class clazz, Throwable cause) {
        this(clazz, null, cause);
    }

    public EntityException(int code, Object... args) {
        super(code, args);
    }

    public EntityException(Throwable cause, int code, Object... args) {
        super(cause, code, args);
    }

    public EntityException(String defaultMessage, int code, Object... args) {
        super(defaultMessage, code, args);
    }

    public EntityException(String defaultMessage, Throwable cause, int code, Object... args) {
        super(defaultMessage, cause, code, args);
    }
}
