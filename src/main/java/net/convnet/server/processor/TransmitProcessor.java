package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@Service
public class TransmitProcessor extends AbstractProcessor {

    @Override
    public Cmd accept() {
        return Cmd.SERVER_TRANS;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        response.setOutput(false);
        Session session1 = sessionManager.getSession(request.getIntParam("userId"));
        if (session1==null)
        {
            //通知用户该用户不在线
            Response resposne1 = createResponse(session,Cmd.OFFLINE_TELL_RESP);
            resposne1.setAttr("who",request.getIntParam("userId"));
            write(session,resposne1);
        }
        else
        {
            write(request.getIntParam("userId"), response.getCmd(), attr("payload", request.getRawParam("payload")));
        }
    }
}
