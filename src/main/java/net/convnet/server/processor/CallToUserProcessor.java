package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.*;
import net.convnet.server.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@Service
public class CallToUserProcessor extends AbstractProcessor implements ApplicationContextAware, InitializingBean {
    @Value("#{props.forceTrans}")
    private boolean forceTrans;


    private static final Logger LOG = LoggerFactory.getLogger(CallToUserProcessor.class);
    private ApplicationContext appCtx;
    private Map<P2PCallType, CallProcessor> callProcessors = new HashMap<P2PCallType, CallProcessor>();

    public void setCallProcessors(Map<P2PCallType, CallProcessor> callProcessors) {
        this.callProcessors = callProcessors;
    }

    @Override
    public Cmd accept() {
        return Cmd.CALL_TO_USER;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        int count = request.getIntParam("count");
        int targetUserId = request.getIntParam("id");
        String password = request.getParam("password");
        User targetuser = userManager.getUser(targetUserId);
        User caller = session.getUser();
        Session targetsession = sessionManager.getSession(targetUserId);
        response.setOutput(false);
        if (targetsession==null)
        {
            //通知用户该用户不在线
            Response resposne1 = createResponse(session,Cmd.OFFLINE_TELL_RESP);
            resposne1.setAttr("who",targetUserId);
            write(session,resposne1);
            return;
        }

        if (forceTrans){
            count=6;
        }

        if (CheckPass(targetuser,password)) {
            //
            if (count < 2)//双方身处同一个局域网的情况
            {
                if (session.getIp().equals(targetsession.getIp())) {
                    Response response1= createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response1.setAttr("callType",P2PCallType.SAMEIP_CALL);
                    response1.setAttr("id", caller.getId());
                    write(targetsession,response1);
                    return;
                }
            }

            if ((targetsession.getAttr("udpport")==null) || (targetsession.getAttr("udpport").equals(""))){
                targetsession.setAttr("udpport","0");
            }

            if ((targetsession.getAttr("tcpport")==null) || (targetsession.getAttr("tcpport").equals(""))){
                targetsession.setAttr("tcpport","0");
            }


            if ((session.getAttr("udpport")==null) || (session.getAttr("udpport").equals(""))){
                session.setAttr("udpport","0");
            }

            if ((session.getAttr("tcpport")==null) || (session.getAttr("tcpport").equals(""))){
                session.setAttr("tcpport","0");
            }

            if (targetsession.getAttr("nattype")==null)
            {
                targetsession.setAttr("nattype","UK");
            }

            if (session.getAttr("nattype")==null)
            {
                session.setAttr("nattype","UK");
            }

            if (count < 3)//处理双方都有外网端口的情况
            {
                if (
                        !session.getAttr("nattype").equals("UK")   //呼叫者可以使用UDP
                                &&
                         (Integer.parseInt(targetsession.getAttr("udpport")) > 0)  //对方有UPNP UDP映射端口
                        &&
                         (Integer.parseInt(session.getAttr("udpport")) > 0)
                        )
                {

                    Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response1.setAttr("callType",P2PCallType.UDP_S2S);
                    response1.setAttr("callerid",caller.getId());
                    response1.setAttr("callerip",session.getIp());
                    response1.setAttr("callerudpport",session.getAttr("udpport"));
                    response1.setAttr("callermac",session.getMac());
                    write(targetsession,response1);

                    Response response2=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response2.setAttr("callType",P2PCallType.UDP_S2S);
                    response2.setAttr("callerid",targetUserId);
                    response2.setAttr("callerip",targetsession.getIp());
                    response2.setAttr("callerudpport",targetsession.getAttr("udpport"));
                    response2.setAttr("callermac",targetsession.getMac());
                    write(session,response2);
                    return;
                }
            }

            if (count < 3)//处理PEER双方有任意一个人有外网端口的状况
            {
                if (
                        !targetsession.getAttr("nattype").equals("UK")   //情况1对方可以使用UDP
                                &&
                          (Integer.parseInt(session.getAttr("udpport")) > 0)  //我有UPNP UDP映射端口
                        )
                {
                    Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response1.setAttr("callType",P2PCallType.UDP_C2S);
                    response1.setAttr("callerid",caller.getId());
                    response1.setAttr("callerip",session.getIp());
                    response1.setAttr("callerudpport",session.getAttr("udpport"));
                    response1.setAttr("callermac",session.getMac());
                    write(targetsession,response1);
                    return;
                }

                if ( !session.getAttr("nattype").equals("UK")   //情况2我可以使用UDP
                        &&
                        (Integer.parseInt(targetsession.getAttr("udpport")) > 0)  //对方有UPNP UDP映射端口
                        )
                {
                    Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response1.setAttr("callType",P2PCallType.UDP_C2S);
                    response1.setAttr("callerid",targetUserId);
                    response1.setAttr("callerip",targetsession.getIp());
                    response1.setAttr("callerudpport",targetsession.getAttr("udpport"));
                    response1.setAttr("callermac",targetsession.getMac());
                    write(session,response1);
                    return;
                }
            }  //end双方有任意一个人有外网端口的状况

            if (count<3)//双方都不能打开外网服务端口，但是有ConeNat
            {
               if (!session.getAttr("nattype").equals("UK")
                       &&
                       !targetsession.getAttr("nattype").equals("UK")
                       )
               {   //通知双方准备端口
                   Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                   response1.setAttr("callType",P2PCallType.UDP_GETPORT);
                   response1.setAttr("callerid",targetUserId);
                   response1.setAttr("callermac",targetsession.getMac());
                   write(session,response1);

                   Response response2=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                   response2.setAttr("callType",P2PCallType.UDP_GETPORT);
                   response2.setAttr("callerid",caller.getId());
                   response2.setAttr("callermac",session.getMac());
                   write(targetsession,response2);
                   return;
               }

            }
            if (count<=5)//TCP
            {
                //我有TCP则通知连我，对方有则连对方
                if (Integer.parseInt(session.getAttr("tcpport")) > 0)  //我有UPNP TCP映射端口
                {
                    Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response1.setAttr("callType",P2PCallType.TCP_C2S);
                    response1.setAttr("callerid",caller.getId());
                    response1.setAttr("callerip",session.getIp());
                    response1.setAttr("callertcpport",session.getAttr("tcpport"));
                    response1.setAttr("callermac",session.getMac());
                    write(targetsession,response1);
                    return;
                }

                if (Integer.parseInt(targetsession.getAttr("tcpport")) > 0)  //对方有UPNP UDP映射端口

                {
                    Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                    response1.setAttr("callType",P2PCallType.TCP_C2S);
                    response1.setAttr("callerid",targetUserId);
                    response1.setAttr("callerip",targetsession.getIp());
                    response1.setAttr("callertcpport",targetsession.getAttr("tcpport"));
                    response1.setAttr("callermac",targetsession.getMac());
                    write(session,response1);
                    return;
                }
            }

            if (count>5){
                response.setOutput(true);
                response.setAttr("callType", P2PCallType.TCP_SvrTrans);
                response.setAttr("id", targetUserId);
                response.setAttr("mac", targetsession.getMac());
                Response response1=createResponse(targetsession,Cmd.CALL_TO_USER_RESP);
                response1.setAttr("callType", P2PCallType.TCP_SvrTrans);
                response1.setAttr("id", session.getUserId());
                response1.setAttr("mac", session.getMac());
                write(targetsession,response1);
                //write(targetUserId, response.getCmd(), attr("count", "11").set("id", session.getUserId()).set("mac", session.getMac()));
            }
        }else //needpass
        {
            //通知用户需要密码
            Response resposne1 = createResponse(session,Cmd.USER_NEED_PASS);
            resposne1.setAttr("id",targetUserId);
            write(session,resposne1);
            return;

        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appCtx = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (CallProcessor callProcessor : appCtx.getBeansOfType(CallProcessor.class).values()) {
            callProcessors.put(callProcessor.accept(), callProcessor);
        }
    }
    public boolean CheckPass(User targetuser,String password)
    {
        String p1=targetuser.getAllowpass1();
        String p2=targetuser.getAllowpass2();

        if ((p1==null) && (p2==null))
        {
            return true;
        }

        if (StringUtils.equals(p1,"") &&StringUtils.equals(p2,""))
        {
            return true;
        }

        if (StringUtils.equals(p1,password) && ! StringUtils.equals(p1,""))
        {
            return true;
        }

        if (StringUtils.equals(p2,password) && ! StringUtils.equals(p2,""))
        {
            return true;
        }
        return false;
    }
}
