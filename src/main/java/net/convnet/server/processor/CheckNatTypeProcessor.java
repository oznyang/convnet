package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.*;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class CheckNatTypeProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.CHECK_NAT_TYPE;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {

        int udpport=request.getIntParam("udpport");
        int tcpport=request.getIntParam("tcpport");

        //TCP
        Socket socket=new Socket();
        try {
            socket.connect(new InetSocketAddress(session.getIp(), tcpport), 5000);
            Writer writer = new PrintWriter(socket.getOutputStream());
            writer.write("Hello Convnet\n");
            writer.flush();


        } catch (IOException e) {
           // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        //UDP
        try {
            DatagramSocket clientSocket = new DatagramSocket();
            //byte[] sendData = new byte[1024];
            //BufferedReader inFromUser = new BufferedReader( new InputStreamReader( System.in ) );
            //String sentence = inFromUser.readLine();
            //sendData = sentence.getBytes();
            String cmd= Cmd.IS_CLIENT_UDP.ordinal() +",ConVnet";
            InetAddress ineta= InetAddress.getByName(session.getIp());
            DatagramPacket sendPacket = new DatagramPacket(cmd.getBytes(), cmd.getBytes().length, ineta, udpport);
            clientSocket.send(sendPacket);
        } catch (SocketException e) {
           // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
           // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        //tcpsend : "Hello Convnet"
        //udpsend : Cmd.cmdISClientUDP+"ConVnet";

    }
}
