package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class KickoutGroupProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.KICK_OUT;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {

        User optuser =userManager.getUser(request.getIntParam("userid"));
        Group group = groupManager.getGroup(request.getIntParam("groupid"));
        response.setOutput(false);
        //不是管理员则什么都不干
        if (session.getUser()!=group.getCreator())
        {
            return;
        }


        for (User user:group.getUsers())
        {
            Session usersession=sessionManager.getSession(user.getId());
            //通知全组
            if (usersession!=null){

            Response response1 = createResponse(usersession, Cmd.KICK_OUT_RESP);
            response1.setAttr("groupid",group.getId());
            response1.setAttr("userid",optuser.getId());
            write(usersession,response1);
            }
        }

        //删除用户
        groupManager.quitGroup(optuser,group);



    }
}
