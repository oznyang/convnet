package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class ModifyGroupProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.MODIFY_GROUP;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {

        {
            Group group1= groupManager.getGroup(request.getIntParam("groupid"));
            if  (!StringUtils.equals(request.getParam("pass"),"NOCHANGE"))
            {
                group1.setPassword(request.getParam("pass"));
            }
            group1.setDescription(request.getParam("desc"));
            groupManager.saveGroup(group1);
            response.setAttr("groupid",request.getIntParam("groupid"));
        }
       // write(session,response);
    }
}
