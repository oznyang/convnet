package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.*;
import net.convnet.server.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@Service
public class GetUserInfoProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.GET_USERINFO;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        //response.setOutput();
        User user=userManager.getUser(request.getIntParam("userid"));
        response.setAttr("userdesc",user.getDescription());
    }


}
