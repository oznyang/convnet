package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class DeleteFriendProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.DEL_FRIEND;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {


        int targetUserId = request.getIntParam("id");
        User targetuser=userManager.getUser(targetUserId);
        User user= session.getUser();

        //删除用户好友信息
        targetuser.getFriends().remove(user);
        user.getFriends().remove(targetuser);
        userManager.saveUser(user);
        userManager.saveUser(targetuser);

        //通知发起用户
        response.setAttr("id",targetUserId);

        //通知被删除用户
        Session targetsession=sessionManager.getSession(targetUserId);
        if (targetsession!=null)
        {
        Response response1 = createResponse(targetsession,Cmd.DEL_FRIEND_RESP);
        response1.setAttr("id",user.getId());
        write(targetsession,response1);
        }

    }
}
