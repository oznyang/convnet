package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class PeerSendMsgProcessor extends AbstractProcessor {

    @Override
    public Cmd accept() {
        return Cmd.SEND_MSGTO_ID;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        int targetuserid = request.getIntParam("userid");
        User targetuser = userManager.getUser(targetuserid);
        Session targetsession = sessionManager.getSession(targetuserid);
        response.setOutput(false);
        if (targetsession==null)
        {
            Response response1 = createResponse(session,Cmd.MSG_CAN_TARRIVE);
            response1.setAttr("userid",targetuserid);
            response1.setAttr("msg",request.getParam("msg"));
            write(session, response1);
        }
        else
        {
            Response response1 = createResponse(targetsession,Cmd.SEND_MSGTO_ID_RESP);
            response1.setAttr("userid", session.getUserId());
            response1.setAttr("msg", request.getParam("msg"));
            write(targetsession,response1);
        }

    }


}
