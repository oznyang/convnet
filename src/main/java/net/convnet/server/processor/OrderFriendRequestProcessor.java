package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class OrderFriendRequestProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.PEER_ORD_FRIEND;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        int targetUserId = request.getIntParam("id");
        String description = request.getParam("description");
        User user=session.getUser();
        int userId= user.getId();
        User targetuser = userManager.getUser(targetUserId);

        response.setOutput(false);

        //如果密码匹配则直接通过
        if (StringUtils.equals(targetuser.getFriendpass(),request.getParam("description")))
        {
            //如果好友关系已经存在，则不作处理
            if (!user.getFriends().contains(targetuser))
            {
                user.getFriends().add(targetuser);
                targetuser.getFriends().add(user);
                userManager.saveUser(user);
                userManager.saveUser(targetuser);
            }else {return;}

            Response response1= createResponse(session,Cmd.PEER_SURE_FRIEND_RESP);
            response1.setAttr("id",targetUserId);
            response1.setAttr("name", targetuser.getName());
            response1.setAttr("isonline", sessionManager.isOnline(targetUserId)?"T":"F");
            write(session,response1);

            //用户在线则通知上线。
            if (sessionManager.getSession(targetUserId)!=null)
            {
                Session targetSession = sessionManager.getSession(targetUserId);
                Response response2= createResponse(targetSession,Cmd.PEER_SURE_FRIEND_RESP);
                response2.setAttr("id",userId);
                response2.setAttr("name", user.getName());
                response2.setAttr("isonline", sessionManager.isOnline(userId)?"T":"F");
                write(targetSession,response2);
            }
            return;
        }

        userManager.sendFriendRequest(session.getUserId(), targetUserId, request.getParam("description"));
        if (sessionManager.isOnline(targetUserId)) {
            Session targetSession = sessionManager.getSession(targetUserId);
            Response notify = createResponse(targetSession, Cmd.PEER_ORD_FRIEND_RESP);
            notify.setAttr("count", "1")
                    .setAttr("userId", user.getId())
                    .setAttr("nickName", user.getNickName())
                    .setAttr("description", description);
            write(targetSession, notify);
        }
    }
}
