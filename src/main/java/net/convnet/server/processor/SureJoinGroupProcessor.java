package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class SureJoinGroupProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.PEER_SURE_JOIN_GROUP;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        response.setOutput(false);


        User orderUser=sessionManager.getSession(request.getIntParam("userid")).getUser();
        groupManager.dealGroupRequest(request.getIntParam("userid"), request.getIntParam("groupid"),false);

        int orderUserid=orderUser.getId();
        if (request.getParam("isallow").equals("T"))
        {
            //加入组
            Group group= groupManager.getGroup(request.getIntParam("groupid"));
            //如果不是管理员则不执行
            if (!group.getAdmins().contains(session.getUser()))
                return;

            groupManager.joinGroup(orderUser,group);

            for (User groupuser:group.getUsers())
            {
                //通知组内成员有新成员加入
                int userid=groupuser.getId();
                if (sessionManager.isOnline(userid))
                {
                    Session targetSession = sessionManager.getSession(userid);
                    Response notify = createResponse(targetSession, Cmd.PEER_SURE_JOIN_GROUP_RESP);
                    notify.setAttr("groupid",group.getId());
                    notify.setAttr("whoid",orderUserid);
                    notify.setAttr("name",orderUser.getName());
                    notify.setAttr("isonline",sessionManager.isOnline(orderUser.getId())?"T":"F");
                    System.out.print("userjoinnotify:"+userid);
                    write(targetSession, notify);
                }
            }
        }

    }
}
