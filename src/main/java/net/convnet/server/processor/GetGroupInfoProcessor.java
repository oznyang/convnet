package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@Service
public class GetGroupInfoProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.GET_GROUP_DESC;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        //response.setOutput();
        Group group = groupManager.getGroup(Integer.parseInt(request.getParam("groupid")));
        response.setAttr("groupdesc",group.getDescription());
    }


}
