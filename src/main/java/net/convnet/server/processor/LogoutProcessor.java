package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class LogoutProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.LOGOUT;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        User user = userManager.getUser( session.getUserId());
        //TellUserOffline(user);
        if (!session.isLogin()) {
            throw new ConvnetException("Not logined");
        }
        session.destory();
    }

    private void TellUserOffline(User user)
    {
      /*  int  userid=user.getId();

        for (User tmpuser : user.getFriends()){
                Session session = sessionManager.getSession(tmpuser.getId());
                if (session!=null){
                Response resposne = createResponse(session,Cmd.OFFLINE_TELL_RESP);
                resposne.setAttr("who",userid);
                write(session,resposne);
                }
        }

        for (Group group:user.getGroups())
        {
            for (User user1:group.getUsers())
            {
                Session session1=sessionManager.getSession(user1.getId());
                if (session1!=null){
                Response response1=createResponse(session1,Cmd.OFFLINE_TELL_RESP);
                response1.setAttr("who",userid);
                write(session1, response1);
                }
            }
        }*/
    }
}
