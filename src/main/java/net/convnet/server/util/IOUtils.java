package net.convnet.server.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.convnet.server.Constants;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
public final class IOUtils {

    public static void writeString(ByteBuf buff, String str) {
        buff.writeBytes(toByteBuf(str));
    }

    public static ByteBuf toByteBuf(String str) {
        return Unpooled.copiedBuffer(str, Constants.CHARSET);
    }

    private IOUtils() {
    }
}
