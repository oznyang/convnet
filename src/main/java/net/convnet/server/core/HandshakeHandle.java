package net.convnet.server.core;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufProcessor;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import net.convnet.server.Constants;
import net.convnet.server.util.IOUtils;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@ChannelHandler.Sharable
final class HandshakeHandle extends ChannelInboundHandlerAdapter {
    private static AttributeKey<Boolean> HANDSHAKE_KEY = new AttributeKey<Boolean>("Handshake");
    private static final Logger LOG = LoggerFactory.getLogger(HandshakeHandle.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf in = (ByteBuf) msg;
        if (!in.isReadable()) {
            return;
        }
        if (LOG.isDebugEnabled()) {
            final StringBuilder sb = new StringBuilder(in.readableBytes());

            sb.append("\n---------------------------------------------------------------------------------------------\n");
            in.forEachByte(new ByteBufProcessor() {
                @Override
                public boolean process(byte value) throws Exception {
                    sb.append(Hex.encodeHex(new byte[]{value})).append(" ");
                    return true;
                }
            });
            sb.append("\n----------------------------------------------------------------------------------------------\n");
            LOG.info("Packet |" + sb.toString() + "|");
            LOG.info("Packet string >>|" + in.toString(Constants.CHARSET) + "|<<");
            sb.append("\n----------------------------------------------------------------------------------------------\n");
        }
        /* byte c = in.getByte(0);

        if (c == 'C') {
            Attribute<Boolean> handshaked = ctx.channel().attr(HANDSHAKE_KEY);
            if (handshaked.get() == null) {
                handshaked.set(Boolean.TRUE);
                ctx.writeAndFlush(IOUtils.toByteBuf("Convnet Connection Accept\n"));
                return;
            }
        }
        */
        ctx.fireChannelRead(msg);
    }
}
