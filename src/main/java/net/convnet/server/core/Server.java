package net.convnet.server.core;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.Lifecycle;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */
public final class Server implements Lifecycle, InitializingBean, DisposableBean {
    private static final Logger LOG = LoggerFactory.getLogger(Server.class);
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private boolean running;

    private ServerInitializer serverInitializer;
    private String serverAddress;
    private int serverPort;
    private int poolSize;

    public void setServerInitializer(ServerInitializer serverInitializer) {
        this.serverInitializer = serverInitializer;
    }

    public void setListen(String listen) {
        String[] arr = StringUtils.split(listen, ":");
        serverAddress = arr[0];
        serverPort = Integer.parseInt(arr[1]);
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    @Override
    public void start() {
        if (running) {
            LOG.info("Already running");
            return;
        }
        LOG.info("------ Attempt to start convnet server on [{}:{}] ------", serverAddress, serverPort);

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class);
        bootstrap.childHandler(serverInitializer);
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        try {
            bootstrap.bind(serverAddress, serverPort).sync();

            LOG.info("------ Convnet server listening on [{}:{}] ready to serve ------", serverAddress, serverPort);
            running = true;
        } catch (Throwable e) {
            LOG.error("Server startup error", e);
        }
    }

    @Override
    public void stop() {
        if (running) {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
            LOG.info("---- Convnet server shutdown successfully ----", serverAddress, serverPort);
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public void destroy() throws Exception {
        stop();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup(poolSize);
        start();
    }
}
