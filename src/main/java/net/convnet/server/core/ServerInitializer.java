package net.convnet.server.core;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
//import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import io.netty.util.HashedWheelTimer;
import net.convnet.server.protocol.Filter;
import net.convnet.server.protocol.FilterChain;
import net.convnet.server.protocol.Processor;
import net.convnet.server.protocol.ProtocolFactory;
import net.convnet.server.session.SessionManager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.ArrayList;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public final class ServerInitializer extends ChannelInitializer<SocketChannel> implements ApplicationContextAware, InitializingBean, DisposableBean {
    private ApplicationContext appCtx;
    private ProtocolFactory protocolFactory;
    private SessionManager sessionManager;
    private final List<Filter> filters = new ArrayList<Filter>();
    private ChannelHandler hibernateSessionHandle;

    private FilterChain chain;
    private ChannelHandler handshakeHandle;
    private ChannelHandler encoderHandle;

    public void setProtocolFactory(ProtocolFactory protocolFactory) {
        this.protocolFactory = protocolFactory;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public void setFilters(List<Filter> filters) {
        this.filters.addAll(filters);
    }

    public void setHibernateSessionHandle(ChannelHandler hibernateSessionHandle) {
        this.hibernateSessionHandle = hibernateSessionHandle;
    }


    @Value("#{props.defaultUserLimitSend}")
    private int defaultUserLimitSend;


    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("readTimeOut",new ReadTimeoutHandler(600));
        pipeline.addLast("handshake", handshakeHandle);
        pipeline.addLast("speedLimit",new ChannelTrafficShapingHandler(defaultUserLimitSend, 0));
        pipeline.addLast("decoder", new PacketDecoder(sessionManager));
        pipeline.addLast("hibernateSession", hibernateSessionHandle);
        pipeline.addLast("encoder", encoderHandle);
        pipeline.addLast("handler", new ConvnetHandler(protocolFactory, sessionManager, chain));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appCtx = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        filters.add(new ProcessFilter(appCtx.getBeansOfType(Processor.class).values()));
        for (Filter filter : filters) {
            filter.init();
        }
        chain = new DefaultFilterChain(filters);
        handshakeHandle = new HandshakeHandle();
        encoderHandle = new PacketEncoder(protocolFactory);
    }

    @Override
    public void destroy() throws Exception {
        for (Filter filter : filters) {
            filter.destroy();
        }
    }
}
