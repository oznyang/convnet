package net.convnet.server.core;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.protocol.*;
import net.convnet.server.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
final class ProcessFilter extends AbstractFilter {
    private static final Logger LOG = LoggerFactory.getLogger(ProcessFilter.class);
    private final Map<Cmd, Processor> processors = new HashMap<Cmd, Processor>();

    ProcessFilter(Collection<Processor> processors) {
        for (Processor processor : processors) {
            this.processors.put(processor.accept(), processor);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void doFilter(Session session, Request request, Response response, FilterChain chain) throws ConvnetException {
        Processor processor = processors.get(request.getCmd());
        if (processor != null) {
            processor.process(session, request, response);
        } else {
            LOG.debug("Processor for cmd [" + request.getCmd() + "] not found");
        }
        chain.doFilter(session, request, response);
    }
}
