package net.convnet.server.web;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-27
 */
public abstract class BaseController {
    protected void success(RedirectAttributes ra) {
        ra.addFlashAttribute("ret", true);
    }

    protected void failed(RedirectAttributes ra, String msg) {
        ra.addFlashAttribute("msg", msg);
        ra.addFlashAttribute("ret", false);
    }
}
