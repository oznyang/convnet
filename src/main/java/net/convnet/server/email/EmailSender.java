package net.convnet.server.email;

import java.util.concurrent.Future;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-28
 */
public interface EmailSender {

    Future<Email> send(Email email);
}
