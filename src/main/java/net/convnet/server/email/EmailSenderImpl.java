/*
 Project:  scc
 Module:   scc-message-service
 File:     EmailSenderImpl.java
 Modifier: oznyang
 Modified: 2013-04-10 20:04

 Copyright (c) 2013 Wisorg Ltd. All Rights Reserved.

 Copying of this document or code and giving it to others and the
 use or communication of the contents thereof, are forbidden without
 expressed authority. Offenders are liable to the payment of damages.
 All rights reserved in the event of the grant of a invention patent
 or the registration of a utility model, design or code.
 */
package net.convnet.server.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.activation.DataSource;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-28
 */
public class EmailSenderImpl implements EmailSender {
    private static Logger LOG = LoggerFactory.getLogger(EmailSenderImpl.class);
    private JavaMailSender mailSender;
    private String from;
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public void setFrom(String from) {
        this.from = from;
    }

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public Future<Email> send(final Email email) {
        return executor.submit(new Callable<Email>() {
            @Override
            public Email call() throws Exception {
                try {
                    doSend(email);
                    if (email.getCallback() != null) {
                        email.getCallback().complete(email);
                    }
                } catch (Throwable e) {
                    LOG.error("Error to send email to " + Arrays.toString(email.getTo()) + " " + e.getMessage());
                    email.setFeedback(e.getMessage());
                    if (email.getCallback() != null) {
                        email.getCallback().error(email);
                    }
                }
                return email;
            }
        });
    }

    private void doSend(Email email) throws Exception {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mailMessage, true, "UTF-8");
        message.setTo(email.getTo());
        if (email.getFrom() != null) {
            message.setFrom(email.getFrom(), email.getFromLabel());
        } else {
            message.setFrom(from, email.getFromLabel());
        }
        if (email.getCc() != null) {
            message.setCc(email.getCc());
        }
        if (email.getBcc() != null) {
            message.setBcc(email.getBcc());
        }
        if (email.getReplyTo() != null) {
            message.setReplyTo(email.getReplyTo());
        }
        if (email.getSentDate() != null) {
            message.setSentDate(email.getSentDate());
        }
        message.setPriority(email.getPriority());
        message.setSubject(email.getSubject());
        message.setText(email.getBody(), true);
        if (email.getAttachment() != null) {
            for (DataSource ds : email.getAttachment()) {
                message.addAttachment(ds.getName(), ds);
            }
        }
        if (email.getInline() != null) {
            for (DataSource ds : email.getInline()) {
                message.addInline(ds.getName(), ds);
            }
        }
        mailSender.send(mailMessage);
    }
}
