package net.convnet.server.session;

import io.netty.channel.Channel;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.Protocol;
import org.springframework.data.domain.Page;

import java.util.Collection;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public interface SessionManager {

    Session createAnonymousSession(Channel channel);

    Session createSession(int userId, Channel channel);

    boolean isOnline(int userId);

    Session getSession(int userId);

    Session getSession(Channel channel);

    Protocol getProtocol(Channel channel);

    Protocol getProtocol(Session session);

    Collection<Session> getSessions();

    boolean sendMessageToUser(User user,String message);

}
