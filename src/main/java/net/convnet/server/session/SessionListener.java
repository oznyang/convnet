package net.convnet.server.session;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
public interface SessionListener {

    void onCreate(Session session);

    void onDestroy(Session session);
}
