package net.convnet.server.session.impl;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.util.internal.PlatformDependent;
import net.convnet.server.identity.*;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Protocol;
import net.convnet.server.protocol.ProtocolFactory;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import net.convnet.server.session.SessionListener;
import net.convnet.server.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
public final class SessionManagerImpl implements SessionManager, DisposableBean {
    private static final Logger LOG = LoggerFactory.getLogger(SessionManagerImpl.class);
    private Map<Integer, Session> sessions = PlatformDependent.newConcurrentHashMap();

    private List<SessionListener> listeners = new ArrayList<SessionListener>();
    private ProtocolFactory protocolFactory;
    private UserManager userManager;


    public void setListeners(List<SessionListener> listeners) {
        this.listeners.addAll(listeners);
    }

    public void setProtocolFactory(ProtocolFactory protocolFactory) {
        this.protocolFactory = protocolFactory;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public Session createAnonymousSession(Channel channel) {
        return new DefaultSession(User.ANONYMOUS_ID, channel) {
            private static final long serialVersionUID = 1326446241742267020L;

            @Override
            public boolean isLogin() {
                return false;
            }

            @Override
            public User getUser() {
                return null;
            }
        };
    }

    @Value("#{props.allUserQuitGroup}")
    private boolean allUserQuitGroup;

    @Autowired
    protected GroupManager groupManager;

    @Override
    public Session createSession(int userId, Channel channel) {

        Session oldSession = sessions.get(userId);
        //发现有重复登录的用户
        if (oldSession != null) {
            //踢出用户，断开连接
            Response kick = getProtocol(oldSession).createResponse(Cmd.LOGIN_RESP);
            //临时使用，Ｄ表示重复登录，并且返回登陆者的ＩＰ；
            kick.setAttr("status", "D")
                    .setAttr("ip", oldSession.getIp());

            oldSession.getChannel().writeAndFlush(kick);
            oldSession.destory();
        }

        DefaultSession session = new DefaultSession(userId, channel) {
            private static final long serialVersionUID = 1326446241742267020L;

            @Override
            public boolean isLogin() {
                return true;
            }

            @Override
            public User getUser() {
                return userManager.getUser(getUserId());
            }

            @Override
            public void destory() {

                //用户离线
                User user = getUser();

                if (isClosed()) {
                    return;
                }

                UserEx userEx= user.getUserEx();

                userEx.setSendToServer(userEx.getSendToServer() + getReadBytes());
                userEx.setReciveFromServer(userEx.getReciveFromServer() + getWriteBytes());
                userEx.setUserIsOnline(false);
                userManager.saveUserEx(userEx);

                final long id = getId();
                final int userId = getUserId();

                //处理同一个用户连续在两个connection中出现后被踢出用户发出离线包
                //导致后已登录用户在线状态不正确的问题
                if (sessions.get(userId) == this) {
                    if (userId > 0) {
                        //通知好友
                        for (User tmpuser : user.getFriends()) {
                            Session session1 = getSession(tmpuser.getId());
                            if (session1 != null) {
                                Response resposne = getProtocol(session1).createResponse(Cmd.OFFLINE_TELL_RESP);
                                resposne.setAttr("who", userId);
                                session1.getChannel().writeAndFlush(resposne);
                            }
                        }

                        //通知组用户，会有重复通知，忽略冗余通知的消耗
                        List<Group> groups=new ArrayList<Group>(user.getGroups());
                        for (Group group : groups) {
                            for (User user1 : group.getUsers()) {
                                int useridtmpid = user1.getId();
                                if (userId != useridtmpid)//忽略本人
                                {
                                    Session session1 = getSession(user1.getId());
                                    if (session1 != null) {
                                        //如果是临时组，离线的用户自动退出组
                                        if (group.getName().startsWith("@") || allUserQuitGroup) {

                                            //管理员退出不操作，其他人退出都通知退出组
                                            if (group.getCreator()!=user) {
                                                Response response1 = getProtocol(session1).createResponse(Cmd.QUIT_GROUP_RESP);
                                                response1.setAttr("msgtype", "userquit");
                                                response1.setAttr("groupid", group.getId());
                                                response1.setAttr("userid", userId);
                                                session1.getChannel().writeAndFlush(response1);
                                            }

                                        }


                                        Response response2 = getProtocol(session1).createResponse(Cmd.OFFLINE_TELL_RESP);
                                        response2.setAttr("who", userId);
                                        session1.getChannel().writeAndFlush(response2);
                                    }
                                }
                            }


                            if (group.getName().startsWith("@") || allUserQuitGroup) {
                                //离线的不是临时组管理员则删除
                                if (group.getCreator() != user) {
                                    groupManager.quitGroup(user, group);
                                }
                            }

                        }
                    }

                }


                for (SessionListener listener : listeners) {
                    listener.onDestroy(this);
                }


                getChannel().close().addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture future) {
                        LOG.debug("Channel [sessionId:" + id + ",userId:" + userId + "] closed");
                    }
                });

                //处理同一个用户连续在两个connection中出现后移除错session的问题
                if (sessions.get(userId) == this) {
                    sessions.remove(userId);
                }

                setClosed(true);
                super.destory();
            }
        };
        sessions.put(userId, session);
        for (SessionListener listener : listeners) {
            try {
                listener.onCreate(session);
            } catch (Exception e) {
                LOG.error("Listener [" + listener + "] onCreate error", e);
            }
        }
        return session;
    }

    @Override
    public boolean isOnline(int userId) {
        return sessions.containsKey(userId);
    }

    @Override
    public Session getSession(int userId) {
        return sessions.get(userId);
    }

    @Override
    public Session getSession(Channel channel) {
        return channel.attr(DefaultSession.ATTR_KEY).get();
    }

    @Override
    public Protocol getProtocol(Session session) {
        int version = session.getProtocolVersion();
        return version > 0 ? protocolFactory.getProtocol(version) : protocolFactory.getDefaultProtocol();
    }

    @Override
    public Protocol getProtocol(Channel channel) {
        return getProtocol(getSession(channel));
    }

    @Override
    public Collection<Session> getSessions() {
        return sessions.values();
    }

    @Override
    public boolean sendMessageToUser(User user, String message) {
        Session session=getSession(user.getId());
        if (session==null)
            return false;
        Protocol protocol=getProtocol(session);
        Response response1 = protocol.createResponse(Cmd.SERVER_SEND_TO_CLIENT);
        response1.setAttr("message",message);
        if (session!=null) {
            session.getChannel().writeAndFlush(response1);
            return true;
        }
        return false;
    }


    @Override
    public void destroy() throws Exception {
        for (Session session : sessions.values()) {
            for (SessionListener listener : listeners) {
                try {
                    listener.onDestroy(session);
                } catch (Exception e) {
                    LOG.error("Listener [" + listener + "] onDestroy error", e);
                }
            }
            session.destory();
        }
        sessions.clear();
    }
}
