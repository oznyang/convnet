package net.convnet.server.identity;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public enum FindType {
    NAME,
    NICK_NAME,
    DESCRIPTION;

    public static FindType from(String s) {
        if ("U".equals(s)) {
            return NAME;
        } else if ("N".equals(s)) {
            return NICK_NAME;
        } else if ("B".equals(s)) {
            return DESCRIPTION;
        }
        return null;
    }
}
