package net.convnet.server.identity;

import net.convnet.server.ex.ConvnetException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public interface UserManager {

    User getUser(int id);

    User getUserByName(String userName);

    List<User> findUser(FindType type, String value, int size);

    Page<User> findUser(String name, Pageable request,boolean isOnline);

    User validateUser(String userName, String password) throws ConvnetException;

    void setPassword(User user, String password);

    void updatePassword(int id, String password);

    User saveUser(User user);

    UserEx saveUserEx(UserEx userEx);

    int getTodayRegistUserCountFromIp(String IP);

    void removeUser(int id);

    void sendFriendRequest(int userId, int targetUserId, String description);

    void dealFriendRequest(int userId, int targetUserId, boolean reject);
}
