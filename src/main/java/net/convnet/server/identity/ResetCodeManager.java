package net.convnet.server.identity;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oznyang@163.com">oznyang</a>
 * @version V1.0, 14-1-14
 */
public interface ResetCodeManager {

    ResetCode findByUserID(int userid);

    ResetCode createResetCode(User user);

    User getUserByResetCode(String code);

    void removeResetCode(String code);
}
