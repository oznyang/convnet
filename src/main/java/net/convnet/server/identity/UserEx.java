package net.convnet.server.identity;

import net.convnet.server.support.attr.Attrable;
import net.convnet.server.support.attr.JSONObjectAttrable;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@Entity
@Table(name = "cvn_user_ex")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserEx extends JSONObjectAttrable implements Attrable {
    private static final long serialVersionUID = -408382392821162139L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date lastLoginAt;
    @Column(length = 32)
    private String lastLoginIp;
    private boolean userIsOnline;
    private long reciveFromServer;
    private long sendToServer;
    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getLastLoginAt() {
        return lastLoginAt;
    }

    public void setLastLoginAt(Date lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isUserIsOnline() {
        return userIsOnline;
    }

    public void setUserIsOnline(boolean userIsOnline) {
        this.userIsOnline = userIsOnline;
    }

    public long getReciveFromServer() {
        return reciveFromServer;
    }

    public void setReciveFromServer(long reciveFromServer) {
        this.reciveFromServer = reciveFromServer;
    }

    public long getSendToServer() {
        return sendToServer;
    }

    public void setSendToServer(long sendToServer) {
        this.sendToServer = sendToServer;
    }

    public String getReciveFromServerString() {
        return FileUtils.byteCountToDisplaySize(reciveFromServer);
    }

    public String getSendToServerString() {
        return FileUtils.byteCountToDisplaySize(sendToServer);
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
