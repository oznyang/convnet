package net.convnet.server.identity.impl;

import net.convnet.server.ex.EntityNotFoundException;
import net.convnet.server.identity.*;
import net.convnet.server.support.hibernate.HibernateRepository;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;



/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-13
 */
@Transactional(readOnly = true)
public class GroupManagerImpl extends HibernateRepository<Group, Integer> implements GroupManager {

    @Autowired
    protected UserManager userManager;

    @Override
    public Group getGroup(int id) {
        return get(id);
    }

    @Override
    public Group getGroupByName(String groupName) {
        return getByNaturalId("name", groupName);
    }

    @Override
    public List<Group> findGroup(FindType type, String value, int size) {
        String fieldName = null;
        switch (type) {
            case NICK_NAME:
                fieldName = "name";
                break;
            case DESCRIPTION:
                fieldName = "description";
                break;
        }
        return list(criteria(Restrictions.like(fieldName, value, MatchMode.ANYWHERE)).addOrder(Order.asc("name")).setMaxResults(size));
    }

    @Override
    @Transactional
    public Group saveGroup(Group group) {
        if (group.getId() == null) {
            group.setCreateAt(new Date());
        }
        return save(group);
    }

    @Override
    @Transactional
    public void removeGroup(int id) {
        deleteByPK(id);
    }


    @Override
    @Transactional
    public void sendGroupRequest(int userId, int targetGroupId, String description) {
        Criteria criteria = getSession().createCriteria(GroupRequest.class);
        criteria.add(Restrictions.eq("user.id", userId));
        criteria.add(Restrictions.eq("target.id", targetGroupId));
        GroupRequest request = (GroupRequest) criteria.uniqueResult();
        if (request == null) {
            request = new GroupRequest();
            request.setCreateAt(new Date());
            request.setTarget(getGroup(targetGroupId));
            request.setUser(userManager.getUser(userId));
        }
        request.setDescription(description);
        getSession().persist(request);
    }

    @Override
    @SuppressWarnings("unchecked")
    public GroupRequest getGroupRequest(int userId, int groupid) {
        Criteria criteria = getSession().createCriteria(GroupRequest.class);
        criteria.add(Restrictions.eq("user.id", userId));
        criteria.add(Restrictions.eq("target.id", groupid));
        return (GroupRequest) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public GroupRequest dealGroupRequest(int userid ,int groupid, boolean reject) {
        //组申请信息
        GroupRequest request = getGroupRequest(userid, groupid);
        if (request == null) {
            throw new EntityNotFoundException(GroupRequest.class, "id=" + userid);
        }

        getSession().delete(request);
        return null;
    }

    @Override
    @Transactional
    public void joinGroup(User user,Group group){
        if (group.getUsers().contains(user))
        {
            return;
        }

        group.getUsers().add(user);
        user.getGroups().add(group);
        saveGroup(group);
        userManager.saveUser(user);
    }

    @Override
    @Transactional
    public void quitGroup(User user,Group group){

        if (group.getUsers().indexOf(user)>=0)
        {
            group.getUsers().remove(user);
            saveGroup(group);
        }

        if (user.getGroups().indexOf(group)>=0)
        {
            user.getGroups().remove(group);
            userManager.saveUser(user);
        }
    }

    @Override
    public List<Group> getAllGroup() {
        return list(criteria().addOrder(Order.desc("id")));
    }
}
