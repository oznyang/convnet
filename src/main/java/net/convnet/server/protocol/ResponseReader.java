package net.convnet.server.protocol;

import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public interface ResponseReader {

    int getVersion();

    Cmd getCmd();

    boolean isSuccess();

    void setOutput(boolean needOutput);

    boolean needOutput();

    public <T> T getAttr(String name);

    public Map<String, Object> getAttrs();
}
