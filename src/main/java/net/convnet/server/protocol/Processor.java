package net.convnet.server.protocol;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.session.Session;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */
public interface Processor {

    Cmd accept();

    void process(Session session, Request request, Response response) throws ConvnetException;
}
