package net.convnet.server.protocol;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public interface ProtocolFactory {

    Protocol getProtocol(int version);

    Protocol getDefaultProtocol();
}
