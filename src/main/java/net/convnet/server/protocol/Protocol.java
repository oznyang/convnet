package net.convnet.server.protocol;

import io.netty.buffer.ByteBuf;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */
public interface Protocol {

    int getVersion();

    String getVersionCode();

    Request decode(ByteBuf buff);

    void encode(ResponseReader reader, ByteBuf buff);

    Response createResponse(Cmd cmd);

    Response exToResponse(Throwable e);
}
