package net.convnet.server.protocol;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.session.Session;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public interface Filter {

    boolean accept(Session session, Request request);

    void doFilter(Session session, Request request, Response response, FilterChain chain) throws ConvnetException;

    void init();

    void destroy();
}
