package net.convnet.server.protocol;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.GroupManager;
import net.convnet.server.identity.UserManager;
import net.convnet.server.session.Session;
import net.convnet.server.session.SessionManager;
import net.convnet.server.util.AttrMap;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public abstract class AbstractProcessor implements Processor {
    @Autowired
    protected UserManager userManager;
    @Autowired
    protected GroupManager groupManager;
    @Autowired
    protected SessionManager sessionManager;

    protected Session getSession(int userId) {
        Session session = sessionManager.getSession(userId);
        if (session == null) {
            throw new ConvnetException("Session for user [" + userId + "] not found");
        }
        return session;
    }

    protected Response createResponse(Session session, Cmd cmd) {
        if (session!=null)
        {
        return sessionManager.getProtocol(session).createResponse(cmd);
        }
        return null;
    }

    protected void write(Session session, Response response) {
        if (session!=null){
            session.getChannel().writeAndFlush(response);
        }
    }

    protected void write(int userId, Cmd cmd, Map<String, Object> attrs) {
        Session session = getSession(userId);
        write(session, createResponse(session, cmd).setAttrs(attrs));
    }

    protected AttrMap attr(String name, Object value) {
        return new AttrMap(name, value);
    }
}
