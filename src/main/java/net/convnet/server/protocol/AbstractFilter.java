package net.convnet.server.protocol;

import net.convnet.server.session.Session;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public abstract class AbstractFilter implements Filter {

    @Override
    public boolean accept(Session session, Request request) {
        return true;
    }

    @Override
    public void init() {
    }

    @Override
    public void destroy() {
    }
}
