package net.convnet.server.protocol.bin;

import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.RequestBuilder;
import net.convnet.server.protocol.ResponseReader;
import org.apache.commons.lang3.ArrayUtils;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public final class CoderMapping implements PacketCoder {
    private Cmd cmd;
    private Cmd rCmd;
    private String[] encode = ArrayUtils.EMPTY_STRING_ARRAY;
    private String[] decode = ArrayUtils.EMPTY_STRING_ARRAY;
    private String star;

    public void setCmd(Cmd cmd) {
        this.cmd = cmd;
    }

    public void setrCmd(Cmd rCmd) {
        this.rCmd = rCmd;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public void setEncode(String[] encode) {
        this.encode = encode;
    }

    public void setDecode(String[] decode) {
        this.decode = decode;
    }

    @Override
    public Cmd getCmd() {
        return cmd;
    }


    @Override
    public Cmd getRespCmd() {
        return rCmd == null ? cmd : rCmd;
    }

    @Override
    public void decode(RequestBuilder builder, BinaryPacket packet) {
        for (int i = 0, len = decode.length; i < len; i++) {
            builder.set(decode[i], packet.get(i));
        }
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        if (encode.length == 0 && star == null) {
            reader.setOutput(false);
            return;
        }
        for (String name : encode) {
            packet.add(reader.getAttr(name));
        }
        if (star != null) {
            packet.end(reader.getAttr(star));
        } else {
            if (packet.IsNeedStar())
                packet.end();
        }
    }
}
