package net.convnet.server.protocol.bin.coder;

import net.convnet.server.identity.Group;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.RequestBuilder;
import net.convnet.server.protocol.ResponseReader;
import net.convnet.server.protocol.bin.AbstractPacketCoder;
import net.convnet.server.protocol.bin.BinaryPacket;
import net.convnet.server.session.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
@Service
public class SendMsgCoder extends AbstractPacketCoder {
    @Autowired
    private SessionManager sessionManager;

    @Override
    public Cmd getCmd() {
        return Cmd.SEND_MSGTO_ID;
    }

    @Override
    public Cmd getRespCmd() {
        return Cmd.SEND_MSGTO_ID_RESP;
    }


    @Override
    public void decode(RequestBuilder builder, BinaryPacket packet) {
        builder.set("userid", packet.get(0));
        builder.set("msg", packet.get(1));
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        packet.add(reader.getAttr("userid"));
        packet.add("*"+reader.getAttr("msg"));
        packet.NeedEndStar(false);
        super.encode(reader, packet);
    }
}
