package net.convnet.server.protocol.bin;

import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.RequestBuilder;
import net.convnet.server.protocol.ResponseReader;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public interface PacketCoder {

    Cmd getCmd();

    Cmd getRespCmd();

    void decode(RequestBuilder builder, BinaryPacket packet);

    void encode(ResponseReader reader, BinaryPacket packet);
}
