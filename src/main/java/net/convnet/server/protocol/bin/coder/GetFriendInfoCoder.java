package net.convnet.server.protocol.bin.coder;

import net.convnet.server.identity.User;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.ResponseReader;
import net.convnet.server.protocol.bin.AbstractPacketCoder;
import net.convnet.server.protocol.bin.BinaryPacket;
import net.convnet.server.session.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
@Service
public class GetFriendInfoCoder extends AbstractPacketCoder {
    @Autowired
    private SessionManager sessionManager;

    @Override
    public Cmd getCmd() {
        return Cmd.GET_FRIEND_INFO;
    }

    @Override
    public Cmd getRespCmd() {
        return Cmd.GET_FRIEND_INFO_RESP;
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        List<User> friends = reader.getAttr("friends");
        for (User user : friends) {
            packet.add(user.getId());
            packet.add(user.getNickName());
            packet.add(sessionManager.getSession(user.getId())!=null);
        }
        super.encode(reader, packet);
    }
}
