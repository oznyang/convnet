package net.convnet.server.protocol.bin.coder;

import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.P2PCallType;
import net.convnet.server.protocol.RequestBuilder;
import net.convnet.server.protocol.ResponseReader;
import net.convnet.server.protocol.bin.AbstractPacketCoder;
import net.convnet.server.protocol.bin.BinaryPacket;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-27
 */
@Service
public class GetUserInfo extends AbstractPacketCoder {
    @Override
    public Cmd getCmd() {
        return Cmd.GET_USERINFO;
    }

    @Override
    public Cmd getRespCmd() {
        return Cmd.GET_USERINFO_RESP;
    }
    //<bean parent="cm" p:cmd="CALL_TO_USER" p:rCmd="CALL_TO_USER_RESP" p:decode="id,count,password" p:encode="p2phead,id,mac"/>
    @Override
    public void decode(RequestBuilder builder, BinaryPacket packet) {
        builder.set("userid", packet.get(0));
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        packet.add("*"+reader.getAttr("userdesc"));
        packet.NeedEndStar(false);
    }
}
