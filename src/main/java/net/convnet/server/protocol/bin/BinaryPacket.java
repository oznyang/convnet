package net.convnet.server.protocol.bin;

import net.convnet.server.protocol.Cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
public final class BinaryPacket {
    private final Cmd cmd;
    private final List<Object> parts;
    private boolean needstar=true;

    public void NeedEndStar(boolean isneedstar)
    {
        needstar=isneedstar;
    }

    public boolean IsNeedStar()
    {
        return needstar;
    }

    public BinaryPacket(Cmd cmd, Object[] parts) {
        this.cmd = cmd;
        this.parts = Arrays.asList(parts);
    }

    public BinaryPacket() {
        this.cmd = null;
        parts = new ArrayList<Object>();
    }

    public Cmd getCmd() {
        return cmd;
    }

    public List<Object> getParts() {
        return parts;
    }

    public Object get(int index) {
        return parts.get(index);
    }

    public BinaryPacket add(Object part) {
        parts.add(part);
        return this;
    }

    public BinaryPacket end(Object part) {
        parts.add(BinaryProtocol.STAR);
        parts.add(part);
        return this;
    }

    public BinaryPacket end() {
        parts.add(BinaryProtocol.STAR);
        return this;
    }
}
