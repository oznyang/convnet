package net.convnet.server.support.attr;

import java.util.Collection;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-2-28
 */
public interface Attrable {

    boolean hasAttr(String key);

    String getAttr(String key);

    String getAttr(String key, String defaultValue);

    String getRequiredAttr(String key) throws IllegalStateException;

    String resolvePlaceholders(String text);

    String[] getArrayAttr(String key);

    <T> T getAttr(String key, Class<T> targetType);

    <T> T getAttr(String key, Class<T> targetType, T defaultValue);

    <T> T getRequiredAttr(String key, Class<T> targetType) throws IllegalStateException;

    String[] getKeys();

    Map<String, String> getAttrs(String... keys);

    Map<String, String> getAttrs(Collection<String> keys);

    void setAttr(String key, Object value);

    void setAttrs(Map<String, ?> map);

    void removeAttr(String key);
}
