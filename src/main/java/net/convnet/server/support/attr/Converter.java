package net.convnet.server.support.attr;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-11-6
 */
public interface Converter {

    <T> T convert(Object value, Class<T> targetType) throws IllegalArgumentException;
}
