package net.convnet.server.support.message;

import java.util.Locale;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-13
 */
public final class NLS {
    private static MessageProvider messageProvider;

    public void setMessageProvider(MessageProvider messageProvider) {
        NLS.messageProvider = messageProvider;
    }

    public static String getMessage(String key) {
        return getMessage(key, null, null, null);
    }

    public static String getMessage(String key, Object[] args) {
        return getMessage(key, args, null, null);
    }

    public static String getMessage(String key, Object[] args, String defaultMessage) {
        return getMessage(key, args, defaultMessage, null);
    }

    public static String getMessage(String key, Object[] args, String defaultMessage, Locale locale) {
        return messageProvider.getMessage(key, args, defaultMessage, locale);
    }

}
