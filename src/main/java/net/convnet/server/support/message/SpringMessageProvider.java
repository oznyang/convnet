package net.convnet.server.support.message;

import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-12
 */
public class SpringMessageProvider implements MessageProvider {

    private MessageSource messageSource;

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public String getMessage(String key) {
        return getMessage(key, null, null, null);
    }

    @Override
    public String getMessage(String key, Object[] args) {
        return getMessage(key, args, null, null);
    }

    @Override
    public String getMessage(String key, Object[] args, String defaultMessage) {
        return getMessage(key, args, defaultMessage, null);
    }

    @Override
    public String getMessage(String key, Object[] args, String defaultMessage, Locale locale) {
        return messageSource.getMessage(key, args, defaultMessage, locale);
    }
}
