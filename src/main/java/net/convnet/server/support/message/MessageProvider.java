package net.convnet.server.support.message;

import java.util.Locale;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-12
 */
public interface MessageProvider {

    String getMessage(String key);

    String getMessage(String key, Object[] args);

    String getMessage(String key, Object[] args, String defaultMessage);

    String getMessage(String key, Object[] args, String defaultMessage, Locale locale);
}
