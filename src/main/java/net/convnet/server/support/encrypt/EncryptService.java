package net.convnet.server.support.encrypt;

import java.security.GeneralSecurityException;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-23
 */
public interface EncryptService {

    String encrypt(String str) throws GeneralSecurityException;

    String decrypt(String str) throws GeneralSecurityException;

    byte[] encrypt(byte[] bytes) throws GeneralSecurityException;

    byte[] decrypt(byte[] bytes) throws GeneralSecurityException;

    String getMethod();
}
