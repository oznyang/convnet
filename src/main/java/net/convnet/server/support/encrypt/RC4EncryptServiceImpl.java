package net.convnet.server.support.encrypt;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-23
 */
public class RC4EncryptServiceImpl extends AbstractEncryptService {
    @Override
    protected Cipher initEncryptCipher(byte[] key) throws GeneralSecurityException {
        SecureRandom sr = new SecureRandom();
        SecretKey sKey = new SecretKeySpec(key, getMethod());
        Cipher cipher = Cipher.getInstance(getMethod());
        cipher.init(Cipher.ENCRYPT_MODE, sKey, sr);
        return cipher;
    }

    @Override
    protected Cipher initDecryptCipher(byte[] key) throws GeneralSecurityException {
        SecureRandom sr = new SecureRandom();
        SecretKey sKey = new SecretKeySpec(key, getMethod());
        Cipher cipher = Cipher.getInstance(getMethod());
        cipher.init(Cipher.DECRYPT_MODE, sKey, cipher.getParameters(), sr);
        return cipher;
    }

    @Override
    public String getMethod() {
        return "RC4";
    }
}
