package net.convnet.server.support.encrypt;

import org.apache.commons.codec.binary.Base64;

import java.security.SecureRandom;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-26
 */
public class Base64Helper {
    public static void main(String[] args) {
        SecureRandom sr = new SecureRandom();
        byte[] bytes = new byte[args.length > 1 ? Integer.valueOf(args[1]) : 16];
        sr.nextBytes(bytes);
        System.out.println(Base64.encodeBase64String(bytes));
    }
}
