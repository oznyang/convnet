package net.convnet.server.support.encrypt;

import net.convnet.server.util.Codecs;
import org.springframework.beans.factory.InitializingBean;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-23
 */
public abstract class AbstractEncryptService implements EncryptService, InitializingBean {
    private Cipher encryptCipher;
    private Cipher decryptCipher;
    private String key;
    private String encryptKey;
    private String decryptKey;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEncryptKey() {
        return encryptKey;
    }

    public void setEncryptKey(String encryptKey) {
        this.encryptKey = encryptKey;
    }

    public String getDecryptKey() {
        return decryptKey;
    }

    public void setDecryptKey(String decryptKey) {
        this.decryptKey = decryptKey;
    }

    @Override
    public String encrypt(String str) throws GeneralSecurityException {
        return Codecs.encode(encrypt(Codecs.getBytes(str)));
    }

    @Override
    public String decrypt(String str) throws GeneralSecurityException {
        return Codecs.toString(decrypt(Codecs.decode(str)));
    }

    @Override
    public byte[] encrypt(byte[] bytes) throws GeneralSecurityException {
        if (encryptCipher == null) {
            throw new IllegalStateException("EncryptCipher not init");
        }
        return encryptCipher.doFinal(bytes);
    }

    @Override
    public byte[] decrypt(byte[] bytes) throws GeneralSecurityException {
        if (decryptCipher == null) {
            throw new IllegalStateException("DecryptCipher not init");
        }
        return decryptCipher.doFinal(bytes);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (encryptKey == null) {
            encryptKey = key;
        }
        if (decryptKey == null) {
            decryptKey = key;
        }
        if (encryptKey != null) {
            encryptCipher = initEncryptCipher(Codecs.decode(encryptKey));
        }
        if (decryptCipher != null) {
            decryptCipher = initDecryptCipher(Codecs.decode(decryptKey));
        }
    }

    protected abstract Cipher initEncryptCipher(byte[] key) throws GeneralSecurityException;

    protected abstract Cipher initDecryptCipher(byte[] key) throws GeneralSecurityException;
}
